import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// routing modules
import { AppRoutingModule } from './app-routing.module';

// components
import { AppComponent } from './app.component';
import { ClanComponent } from './clan/clan.component';
import { MemberComponent } from './member/member.component';

// primeng
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    AppComponent,
    ClanComponent,
    MemberComponent
  ],
  imports: [
    BrowserModule,
    // primeng modules
    ButtonModule,
    TableModule,
    // this module must be imported at last:
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
