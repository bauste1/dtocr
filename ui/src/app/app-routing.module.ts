import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClanComponent } from './clan/clan.component';
import { MemberComponent } from './member/member.component';

const routes: Routes = [
  {
    path: 'clan',
    component: ClanComponent
  },
  {
    path: 'member',
    component: MemberComponent
  },
  {
    path: '**',
    redirectTo: 'clan'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
